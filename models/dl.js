var mongoose = require('mongoose');
var Schema =mongoose.Schema;

var dlSchema = new Schema({
  dlnumber:{type:String,require:true},
  issuer:{type:String,require:true},
  address:{type:String,require:true},
  city:{type:String,require:true,},
  state:{type:String,require:true,},
  country:{type:String,require:true,},
  pin:{type:String,require:true,},
  createdAt:{type:Date,default:Date.now},
  user:{type:String,require:true,}
});

module.exports=mongoose.model('Dl',dlSchema);
