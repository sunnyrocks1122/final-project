var mongoose = require('mongoose');
var Schema =mongoose.Schema;

var vehicleSchema = new Schema({
  regnumber:{type:String,require:true},
  brand:{type:String,require:true},
  model:{type:String,require:true},
  issuer:{type:String,require:true},
  type:{type:String,require:true},
  specs:{
    fuel:{type:String,require:true},
    passengercap:{type:String,require:true},
    cc:{type:String,require:true},
  },
  address:{type:String,require:true},
  city:{type:String,require:true,},
  state:{type:String,require:true,},
  country:{type:String,require:true,},
  pin:{type:String,require:true,},
  createdAt:{type:Date,default:Date.now},
  user:{type:String,require:true,}
});

module.exports=mongoose.model('VS',vehicleSchema);
